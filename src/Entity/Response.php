<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\ResponseRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* @ApiResource(
 *     normalizationContext={"groups"={"response:read"}},
 *     denormalizationContext={"groups"={"response:write"}},
 *     collectionOperations={
 *         "get",
 *         "post"={"path"="/response", "security"="is_granted('ROLE_USER')"}
 *     },
 *     itemOperations={
 *         "get"={"path"="/response/{id}"},
 *         "put"={"path"="/response/{id}",  "security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.author == user and previous_object.author == user)",  "security_message"="Désolé, vous n'etes pas propriétaire de cette réponse."},
 *         "delete"={"path"="/response/{id}", "security_post_denormalize"="is_granted('ROLE_ADMIN') or (object.author == user and previous_object.author == user)",  "security_message"="Désolé, vous n'etes pas propriétaire de cette réponse."}
 *     },
 *     attributes={
 *          "pagination_items_per_page"=9
 *     }
 * )
 * @ApiFilter(OrderFilter::class, properties={"createdAt"}, arguments={"orderParameterName"="order"})
 * @ApiFilter(SearchFilter::class, properties={"author": "exact"})
 * @ORM\Entity(repositoryClass=ResponseRepository::class)
 */
class Response
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"response:read", "post:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"response:read", "response:write", "post:read"})
     * @Assert\NotBlank()
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"response:read", "post:read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"response:read", "post:read"})
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="responses")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"response:read", "response:write", "post:read"})
     */
    private $post;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="responses")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"response:read"})
     */
    public $author;

    public function __construct()
    {
        $this->createdAt = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPost(): ?Post
    {
        return $this->post;
    }

    public function setPost(?Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): self
    {
        $this->author = $author;

        return $this;
    }
}
